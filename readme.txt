To compile and run the code from command line under the source folder from Linux/OSX:
$ ant
$ export CLASSPATH=$CLASSPATH:`pwd`/build/classes 

To run UserDatabase:
$ java secs/cse231/project/user/UserDatabase

To run VFolder:
java secs/cse231/project/vfolder/VFolder
