//----------------------------------------------------------------------------
// DLLNode.java              by Dale/Joyce/Weems                     Chapter 7
//
// Implements <T> nodes for a doubly linked list.
//----------------------------------------------------------------------------

package secs.cse231.project.util;

import java.io.Serializable;

public class DLLNode<T> implements Serializable {
    private T info;
    private DLLNode<T> link;
    private DLLNode<T> back;

    public DLLNode(T info) {
        this.info = info;
        link = null;
        back = null;

    }

    /* a handy constructor */
    public DLLNode(T info, DLLNode<T> link, DLLNode<T> back) {
        this.info = info;
        this.link = link;
        this.back = back;
    }

    public DLLNode<T> getLink() {
        return link;
    }

    public void setLink(DLLNode<T> link) {
        this.link = link;
    }

    public DLLNode<T> getBack() {
        return back;
    }

    public void setBack(DLLNode<T> back) {
        this.back = back;
    }

    public T getInfo() {
        return info;
    }
}
 
 