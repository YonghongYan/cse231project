package secs.cse231.project.vfolder;

import java.io.File;
import java.io.Serializable;

import secs.cse231.project.user.User;
import secs.cse231.project.util.DLLNode;

/**
 * Created by yy8 on 9/18/14.
 *
 * A folder itself is a special file, that is why we extends VFile class.
 *
 * A folder contains a list of files stored in a doubly circular linked list, and the list has a header which is the folder itself.
 * But the folder itself is not part of the contents of the folder. For example, when you search the folder for a file,
 * you should not compare the target with the folder itself.
 *
 * Two files cannot have the same name in the folder. Files are stored in order according to the alphabetic order of file name.
 */
public class VFolder extends VFile implements Serializable {
    private DLLNode<VFile> files;
    private int numFile;

    public VFolder() {
        super();
        numFile = 0;
        files = new DLLNode<VFile>(this);
        files.setBack(files);
        files.setLink(files);
    }

    public VFolder(String name, User owner) {
        super(name, VFileType.FOLDER, owner);
        numFile = 0;
        files = new DLLNode<VFile>(this);
        files.setBack(files);
        files.setLink(files);
    }

    public boolean containsFile(long fileID) {
        DLLNode<VFile> node = files.getLink();
        while (node != files) {
            if (node.getInfo().getFileID() == fileID) return true;
            else node = node.getLink();
        }
        return false;
    }

    public boolean containsFile(String name) {
        DLLNode<VFile> node = files.getLink();
        while (node != files) {
            if (node.getInfo().getName().equals(name)) return true;
            node = node.getLink();
        }
        return false;
    }

    /**
     * check whether a file is in the folder or not
     * @param file
     * @return
     */
    public boolean containsFile(VFile file) {
        return false;
    }

    /**
     * retrieve the file object according to the fileID
     * @param fileID
     * @return
     */
    public VFile getFile(long fileID) {
        return null;
    }

    /**
     * retrieve the file object according to the file name
     * @param name
     * @return
     */
    public VFile getFile(String name) {
        return null;
    }

    /**
     * Add a file to a folder, insert it in the right position of the list according to the order of files (based on their names)
     * @param file: the file object
     * @return true; successful; false: not successful
     */
    public boolean addFile(VFile file) {

        return false;
    }

    /**
     * Remove a file from a folder
     * @param file
     * @return
     */
    public boolean deleteFile (VFile file) {
        return false;
    }

    /**
     * delete a file with the specified ID, and return the deleted file
     * @param fileID
     * @return
     */
    public VFile deleteFile(long fileID) {
        return null;

    }

    public VFile deleteFile(String name) {
        return null;
    }


    public int getNumFile() {
        return numFile;
    }


    /**
     * add a file to the folder in the first position, i.e. not putting it in the right location to maintain the ordering
     * @param file
     */
    public void addFileMess(VFile file) {
        DLLNode<VFile> newNode = new DLLNode<VFile>(file);
        newNode.setLink(files.getLink());
        newNode.setBack(files);

        files.getLink().setBack(newNode);
        files.setLink(newNode);
    }

    /**
     * when you implement the addFile method, you should use it in this function so all the file will be stored in the
     * order we want
     * @param folderName
     */
    public void readLocalFolder(String folderName) {
        File folder = new File(folderName);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            File localFile = listOfFiles[i];
            VFile newFile = null;
            if (localFile.isFile()) {
                newFile = new VFile(localFile.getName(), VFileType.TEXT, this.getOwner());
            } else if (localFile.isDirectory()) {
                newFile = new VFolder(localFile.getName(), this.getOwner());
            }
            if (newFile != null) {
                newFile.setLocation(folderName);
                addFileMess(newFile);
            }

        }
    }

    @Override
    public String toString() {
        String str = this.getName() + "(" + this.getNumFile() + " files): \n";
        DLLNode<VFile> node = files.getLink();
        while (node != files) {
            VFile file = node.getInfo();
            str += "\t" + file + "\n";
            node = node.getLink();
        }
        return str;
    }

    public void test () {
        VFile file = new VFile("myFile1", VFileType.IMAGE_JPEG, null);
        addFile(file);

        VFile file2 = new VFile("myFile2", VFileType.IMAGE_JPEG, null);
        addFile(file2);

        boolean containFile1 = containsFile("myFile1");

        if (containFile1) {
            System.out.println("the folder contains file: myFile1");
        } else {
            System.out.println("the folder does not contain file: myFile1");
        }

        deleteFile("myFile1");
        containFile1 = containsFile("myFile1");
        if (containFile1) {
            System.out.println("the folder contains file: myFile1");
        } else {
            System.out.println("the folder does not contain file: myFile1");
        }
    }

    public static void main(String[] args)
    {
        String homeFolderString = System.getProperty("user.dir");
        User owner = new User("FirstName", "LastName", "id", null);
        VFolder homeFolder = new VFolder(homeFolderString, owner);
        homeFolder.readLocalFolder(homeFolderString);
        System.out.println(homeFolder);

    }


}
