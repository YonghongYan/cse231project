package secs.cse231.project.vfolder;

import secs.cse231.project.user.*;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yy8 on 9/18/14.
 */
public class VFile implements Serializable {
    static long fileID_sequence = 0;
    private long fileID;
    private String name;
    private String extension;
    private String location; /* where is the file is located */
    private VFileType type;

    private String permission; /* read/write/exe */
    private User owner;
    private Group group;
    private VFolder folder;

    private Date creation_time;
    private Date last_access;
    private Date last_write;

    /* default constructor */
    public VFile() {
        this.fileID = fileID_sequence;
        fileID_sequence ++;
        creation_time = new Date();
    }

    public VFile(String name, VFileType type, User owner) {
        this.name = name;
        this.type = type;
        this.owner = owner;

        this.fileID = fileID_sequence;
        fileID_sequence ++;
        creation_time = new Date();
    }

    public long getFileID() {
        return fileID;
    }

    public String getName() {
        return name;
    }

    public String getExtension() {
        return extension;
    }

    public String getLocation() {
        return location;
    }

    public VFileType getType() {
        return type;
    }

    public String getPermission() {
        return permission;
    }

    public User getOwner() {
        return owner;
    }

    public Group getGroup() {
        return group;
    }

    public VFolder getFolder() {
        return folder;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setType(VFileType type) {
        this.type = type;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setFolder(VFolder folder) {
        this.folder = folder;
    }

    public Date getCreation_time() {
        return creation_time;
    }

    public Date getLast_access() {
        return last_access;
    }

    public void setLast_access(Date last_access) {
        this.last_access = last_access;
    }

    public Date getLast_write() {
        return last_write;
    }

    public void setLast_write(Date last_write) {
        this.last_write = last_write;
    }

    @Override
    public String toString() {
        return location+"/"+name+"."+extension;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VFile vFile = (VFile) o;

        if (fileID != vFile.fileID) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (fileID ^ (fileID >>> 32));
    }

}
