package secs.cse231.project.vfolder;

/**
 * Created by yy8 on 9/18/14.
 */
public enum VFileType {
    FOLDER,
    IMAGE_PNG,
    IMAGE_JPEG,
    VIDEO_mpeg,
    VIDEO_avi,
    TEXT,
    BINARY,
    EXE,
}
