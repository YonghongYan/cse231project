package secs.cse231.project.user;

/**
 * Created by yy8 on 10/2/14.
 */

import secs.cse231.project.util.ListInterface;

import java.io.Serializable;

public class Group implements Comparable, Serializable {
    private String name; /* name is the key for sorting group objects and also used for compareTo and equals method */
    ListInterface<User> members;

    public Group(String name) {
        this.name = name;
        /* TODO */
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Object o) {
        return -1;
    }

    @Override
    public boolean equals(Object o) {
        return false;
    }

    /**
     * return true if successful added, false otherwise.
     * It should not allow duplicated users (users with the same id)
     * @param u
     * @return
     */
    public boolean addUser(User u) {
        if (containUser(u)) return false;
        members.add(u);
        return true;
    }

    /**
     * Check to see whether a user u is in this group or not.
     */
    public boolean containUser (User u) {
        return members.contains(u);
    }

    public boolean removeUser (User u) {
        return members.remove(u);
    }

    /**
     * Check to see whether a user u is in this group or not.
     */
    public boolean containUser (String userID) {
        return false;
    }


    public boolean removeUser (String userID) {
        return false;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", members=" + members +
                '}';
    }
}
